import java.util.*;
public class UsingArrayList {
  public static void main(String[] args) {
    List<Integer> list = new ArrayList<Integer>(10);
    list.add(1);
    list.add(2);
    System.out.println(list);
  }
}
