package org.extendj;

import org.extendj.ast.CompilationUnit;

public class ExtensionMain extends JavaChecker {

  public static void main(String args[]) {
    int exitCode = new ExtensionMain().run(args);
    if (exitCode != 0) {
      System.exit(exitCode);
    }
  }

  @Override
  protected int processCompilationUnit(CompilationUnit unit) throws Error {
    if (unit.fromSource()) {
      unit.process();
    }
    return 0;
  }
}
